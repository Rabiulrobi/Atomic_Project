<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107915\book\book;
use ATOMIC12\BITM\seip107915\Message\message;
use ATOMIC12\BITM\seip107915\Utility\Utility;

$book = new Book();
$var =$book->index();

?>
<html>
    <head>
     
        <title>Book Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Book List</h1>
            </center>
        </header>
        <hr>
     <center>
             
   <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                         <div>
                            <?php echo Message::flash();?>.
                          </div> 
                    
  <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th>Serial</th>
        <th>Name</th>
        <th>Author</th>
        <th>Action</th>
      </tr>
    </thead>
            
           <?php foreach($var as $book): ?>
           <tr class="info">
               <td><?php echo $book['id'];?></td>
               <td><?php echo $book['name'];?></td><td><?php echo $book['author'];?>
               </td><td><a class="btn btn-success" href="view.php?id=<?php echo $book['id'];?>">View</a> | <a class="btn btn-primary" href="edit.php?id=<?php echo $book['id'];?>">Edit</a> | <a class="btn btn-danger" href="delete.php?id=<?php echo $book['id'];?>">Delete</a></td></tr>
            <?php endforeach;?>
 </table>
            
       </center>
 </div> 
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
