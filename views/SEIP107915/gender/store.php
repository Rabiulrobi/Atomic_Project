<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107915\gender\Gender;
use ATOMIC12\BITM\seip107915\Message\Message;
use ATOMIC12\BITM\seip107915\Utility\Utility;

$gender = new Gender();
$gender->prepare($_REQUEST)->store();
?>