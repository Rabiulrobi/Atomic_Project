<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107915\city\city;

$city = new city();
$var =$city->edit($_GET['id']);

?>



<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>City</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
        <header>
            <center>
                <h1>Update City</h1>
            </center>
        </header>
        <hr>
        
        <div id="menu">
            <center>
             
             
            <div id="form">
                <ul class="pager">
                  <li class="previous"><a href="index.php">Back</a></li>
                </ul>
            <form method="post" action="update.php" >
                 <table>
                  <input type="hidden" name="id" value="<?php echo $var->id;?>">
                  <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon">Person Name</span>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Person Name" required >
                                </div>
                            </div>   
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Select City</span>
                                            <select class="form-control"name="city"  required>
                                                    <option value="Dhaka">Dhaka</option>
                                                    <option value="Comilla">Comilla</option>
                                                    <option value="Chittagong">Chittagong</option>
                                                    <option value="Barisal">Barisal</option>
                                                    <option value="Khulna">Khulna</option>
                                           </select>
                                </div>
                            </div>
                 
                 <tr><td></td><td><input class="btn btn-success" type="reset"  value="Reset" />
                <input class="btn btn-info" type="submit" name="Update" value="Update" /></td></tr>
                
                
                </table>
                
                
                
            </form>
            
       
            
            </div>
         </center>   
                
                
         
        </div>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>   
    </body>
</html>
