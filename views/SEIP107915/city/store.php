<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107915\city\City;
use ATOMIC12\BITM\seip107915\Message\Message;
use ATOMIC12\BITM\seip107915\Utility\Utility;

$city = new City();
$city->prepare($_REQUEST)->store();

?>